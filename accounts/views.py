from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.models import User
from accounts.forms import LogInForm, SignUpForm


# Create your views here.
def user_login(request):
    if request.method == "POST":
        form = LogInForm(request.POST)
        if form.is_valid():
            user_name = form.cleaned_data["username"]
            pass_word = form.cleaned_data["password"]

            user = authenticate(
                request, username=user_name, password=pass_word
            )
            if user is not None:
                login(request, user)
                return redirect("home")
            else:
                form = LogInForm()
                context = {"form": form}
                return render(request, "accounts/login.html", context)

    else:
        form = LogInForm()
        context = {"form": form}
        return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            user_name = form.cleaned_data["username"]
            pass_word = form.cleaned_data["password"]
            pass_word_confirmation = form.cleaned_data["password_confirmation"]

            if pass_word == pass_word_confirmation:
                user = User.objects.create_user(
                    username=user_name,
                    password=pass_word,
                )
                login(request, user)
                return redirect("home")
            else:
                form.add_error("password", "the passwords do not match")

    else:
        form = SignUpForm()
        context = {"form": form}
        return render(request, "accounts/signup.html", context)
