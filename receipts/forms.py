from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class ReceiptForm(ModelForm):  # Step 1
    class Meta:  # Step 2
        model = Receipt  # Step 3
        fields = [  # Step 4
            "vendor",  # Step 4
            "total",  # Step 4
            "tax",
            "date",
            "category",
            "account",
        ]


class ExpenseCategoryForm(ModelForm):  # Step 1
    class Meta:  # Step 2
        model = ExpenseCategory  # Step 3
        fields = [  # Step 4
            "name",
        ]


class AccountForm(ModelForm):  # Step 1
    class Meta:  # Step 2
        model = Account  # Step 3
        fields = ["name", "number"]  # Step 4
