from django.shortcuts import render, get_object_or_404, redirect
from receipts.models import Receipt
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.db.models import Count

# Create your views here.


@login_required
def get_receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipts/receipts.html", context)


def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            login_required()
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
        context = {"form": form}
        return render(request, "receipts/create.html", context)


def category_list(request):
    categories = (
        Receipt.objects.filter(purchaser=request.user)
        .values("category__name")
        .annotate(count=Count("category"))
        .order_by()
    )
    context = {"categories": categories}
    return render(request, "receipts/categories.html", context)


def account_list(request):
    accounts = (
        Receipt.objects.filter(purchaser=request.user)
        .values("account__name", "account__number")
        .annotate(count=Count("account"))
        .order_by()
    )
    context = {"accounts": accounts}
    return render(request, "receipts/accounts.html", context)


def create_expense_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            login_required()
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
        context = {"form": form}
        return render(request, "receipts/create_categories.html", context)


def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            login_required()
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
        context = {"form": form}
        return render(request, "receipts/create_accounts.html", context)
